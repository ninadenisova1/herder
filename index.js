"use strict";
const express = require('express')
const app = express()
const exphbs  = require('express-handlebars')
const moment = require('moment')
const config = require('config-yml').config;

app.use(express.static('assets'))
app.engine('handlebars', exphbs({defaultLayout: 'main'}))
app.set('view engine', 'handlebars')

app.get('/', (req, res) => {
    const loader = require('./lib/db_loader');
    const dataDecorator = require('./lib/decorator');
    return loader.filter(permitParams(req.query)).then(function (objects) {
        const context = { 
            version: process.version, 
            time: new Date(), 
            map: config.app.map,
            from: req.query.from,
            to: req.query.to,
            device: req.query.device,
            tracking: req.query.tracking ? 'checked="checked"' : '',
            length: objects.length
         };
        context.objectsJSON = dataDecorator(objects);
        return res.render('index', context);
    });
});

app.get('/devices', (req, res) => {
    const currentDevice = req.query.device;
    const loader = require('./lib/db_loader');
    return loader.devices(permitParams(req.query)).then(function (devices) {
        const results = devices.concat('').sort()
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(results));
        return res;
    });
});

function permitParams(query) {
    const res = {}
    if(query.tracking) res.tracking = query.tracking;
    if(query.device) res.device = query.device;
    const from = query.from
    const to = query.to
    const dateTimeFormat = 'YYYY-MM-DD HH:mm'
    if (from) res.from = parseInt(moment(from, dateTimeFormat).valueOf())
    if (to) res.to = parseInt(moment(to, dateTimeFormat).valueOf())
    return res;
}


app.listen(3000, () => console.log('Example app listening on port 3000!'))
