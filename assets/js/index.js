window.onload = function () {
    const elementFrom = document.getElementsByClassName('from')[0]
    const dateTimeFormat = 'Y-m-d H:i'
    const originalDateTimeFormat = 'YYYY-MM-DD HH:mm'
    const searchForm = window.document.getElementById('search')
    const trackingInterval = 60000
    const shiftTime = (value, step, format) => {
        return window.moment(window.moment(value, format).valueOf() + step).format(format)
    }
    
    const submitForm = () => {
        setTimeout( function () {
            searchForm.submit()        
        }, 0)
    }

    const getDevices = () => {
        fetch('/devices' + window.location.search).then(
            function(resp){
                resp.json().then(
                    function(data) {
                        const select = window.document.getElementById('device-label');
                        const current = new URL(location.href).searchParams.get('device');
                        const strData = data.map(function(label){
                            return "<option " + (label == current ? 'selected' : '') + " value='" + label + "'>" + label + "</option>"
                        }).join("")
                        select.innerHTML = strData;
                    }
                )
        });
    }
    getDevices();

    setInterval(function () {
        const tracking = window.document.getElementById('tracking-mode')
        // if (tracking.checked) {
        //     elementFrom.value = shiftTime(elementFrom.value, trackingInterval, originalDateTimeFormat)        
        // }
        submitForm()
    }, trackingInterval);

    const datetimeOptions = {
        timePicker: true,
        timePickerFormat: 24,
        format: dateTimeFormat,
        input: elementFrom,
        startDate: Date.parse(elementFrom.value) || Date.parse("2018-01-01 12:00"),
        onChange: submitForm
    }

    new DateTimePicker('.datetime.from', datetimeOptions);

    const elementTo = document.getElementsByClassName('to')[0]
    const options = {
        timePicker: true,
        timePickerFormat: 24,
        format: dateTimeFormat,
        startDate: Date.parse(elementTo.value) || new Date,
        input: elementTo,
        onChange: submitForm
    }
    new DateTimePicker('.datetime.to', options);

    var ID = 'map'
    var map = L.map(ID).setView([56.85, 60.61], 13);
    var container = window.document.getElementById(ID);

    L.tileLayer(window.MAP_URL, {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    const getPopup = function (object) {
        const label = window.moment(object.timestamp).format('DD.MM.YY HH:mm');
        delete object.timestamp; 
        return "<div class='card-body' style='max-height: 300px; overflow: scroll'>" +
            "<h2>" + label + "</h2>" +
                "<table class='table table-striped '><tbody>" + Object.keys(object).map(function (key) {
                    return "<tr><td><b>" + key.toString() + "</b></td><td>" + object[key].toString() + "</td></td>"    
            }).join("") + "<tbody></table>"
        "</div>"
    }

    const myIcon = L.icon({
        iconUrl: 'images/marker-red.png',
        iconSize: [26, 40],
        iconAnchor: [13, 40]
    });
    L.marker([50.505, 30.57], {icon: myIcon}).addTo(map);
    
    const markers = JSON.parse(container.dataset.points).map(function (object) {
        if (object.activity >= 10) {
            return L.marker([object.lt, object.ln], {icon: myIcon}).bindPopup(getPopup(object), {className: 'card'})    
        }
        return L.marker([object.lt, object.ln]).bindPopup(getPopup(object), {className: 'card'})
    });

    if (markers.length > 0) {
        var group = L.featureGroup(markers).addTo(map);
        map.fitBounds(group.getBounds());
    }
};
