const moment = require('moment')

const decorator = function (objects) {
    const results = objects.map(function (object) {
        object.gps_time = moment(object.gps_time).format('DD.MM.YYYY HH:mm');
        return object;
    });
    return JSON.stringify(results);
}

module.exports = decorator;