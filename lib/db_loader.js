const sqlite3 = require('sqlite3').verbose();
const config = require('config-yml').config;
const moment = require('moment');

const Loader = {
    filter: function (params) {
        let query = "SELECT * FROM logs ";
        query += Loader.composeFiltersQuery(params);
        query += ' order by timestamp desc limit 300'
        return Loader.query(query)
    },

    composeFiltersQuery: function (params) {
        query = "WHERE ( NOT ( lt == 0 AND ln == 0 ) )"
        if (params && Object.keys(params).length > 0) {
            query += " AND "
            const cases = []

            if(!params.tracking) {
                if (params.to) cases.push("timestamp <=  " + params.to.toString())
                if (params.from) cases.push("timestamp >=  " + params.from.toString())
            } else {
                timestamp = moment().add(-1, 'hours').valueOf().toString();
                cases.push("timestamp >=  " + timestamp);
            }
            if (params.device) cases.push('label ==  "' + params.device.toString() + '"') 
            query += cases.join(" AND ")
            if (params.tracking) query += " GROUP BY label";
            return query;
        }

    }, 

    devices: function (params) {
        delete params.device;
        let query = "SELECT DISTINCT label FROM logs "
        return Loader.query(query + Loader.composeFiltersQuery(params)).then(function (objects) {
            devices = []
            objects.forEach((object) => devices.push(object.label))
            return devices;
        })
    },
    
    query: function (query) {
        return new Promise((resolve, reject) => {
            const db = new sqlite3.Database(config.app.db.path);
            db.serialize(function() {
                console.log('QUERY:', query)
                db.all(query, (err, rows) => {
                    if (err) {
                        reject(err); // optional: you might choose to swallow errors.
                    } else {
                        resolve(rows); // accumulate the data
                    }
                    db.close();
                });
            });
        });
    }
}

module.exports = Loader
