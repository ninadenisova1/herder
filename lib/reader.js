const fs = require('fs');

const reader = function (path) {
    return fs.readFileSync(path, 'utf8');
};

module.exports = reader;