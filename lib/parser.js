const moment = require('moment')
const attributes = require('./attributes')

const parser = function (text) {
    const lines =  text.split("\n");
    return lines.map(parseLine).filter(function (obj) {
        return obj.lt || obj.ln
    });
}

const prepareReal = function (str) {
    console.log("prepareReal")
    console.log(str)
    if (str) {
        console.log('str != false')
        console.log(parseFloat(str.replace(",",".")))
        return parseFloat(str.replace(",","."))
    }
    return 0;
}

const prepareInt = function (str) {
    if (str) return parseInt(str)
    return 0;
}

const prepareValue = function (str, type) {
    if (type == "INT") return prepareInt(str);
    if (type == "REAL") return prepareReal(str);
    if (type == "TEXT") return "'" + str + "'";
    return str
}

const parseLine = function (line) {
    const cortege = line.split(' - ')
    const [timetamp, label] = cortege[0].split(": ")

    const data = {
        timestamp: prepareValue(moment(timetamp, 'YYYY/MM/DD HH-mm-ss').valueOf(), attributes.timetamp),
        label: prepareValue(label, attributes.label),
        gps_time: prepareValue(moment(cortege[1], 'YYYY/MM/DD HH-mm-ss').valueOf(), attributes.gps_time)
    }

    cortege.slice(2).forEach(function (pair) {
        const [ key, value ] = pair.split(':')
        const type = attributes[key]
        data[key] = prepareValue(value, type)
    })

    return data;
}

module.exports = parser;
