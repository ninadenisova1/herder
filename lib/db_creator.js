const sqlite3 = require('sqlite3').verbose();
const attributes = require('./attributes')
const config = require('config-yml').config;
console.log(config)

const dbCreator = function () {
    const db = new sqlite3.Database(config.app.db.path);
 
    db.serialize(function() {
        const query = "CREATE TABLE logs (" + Object.keys(attributes).map(function (key) {
            return key + " " + attributes[key]
        }).join(', ')  + ")";

        db.run(query);
    });
    db.close();
};

dbCreator()
