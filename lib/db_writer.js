const sqlite3 = require('sqlite3').verbose();
const reader = require('./reader')
const parser = require('./parser')
const config = require('config-yml').config;

const dbWriter = function (objects) {
    const db = new sqlite3.Database(config.app.db.path);
 
    db.serialize(function() {
        objects.forEach(function (object) {
            const query = "INSERT INTO logs VALUES (" + Object.values(object).join(", ") + ")";
            console.log('query', query);
            const stmt = db.prepare(query);
            stmt.run();
            stmt.finalize();
        });
      });
    db.close();
}

const data = reader(config.app.textdata.path)
const objects = parser(data)
dbWriter(objects)
