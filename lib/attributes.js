const TEXT = 'TEXT'
const INT = "INT"
const REAL = "REAL"

const attributes = {
    timestamp: INT, label: TEXT,
    gps_time: INT, vbat: INT, 
    dig: INT, lt: REAL, ln: REAL, 
    press: REAL, spt: INT, fix: INT, 
    course: INT, speed: REAL,
    activity: INT, HDOB: REAL,
    temp: REAL, rssi2: INT, nois2: INT
}

module.exports = attributes